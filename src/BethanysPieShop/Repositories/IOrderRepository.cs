﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BethanysPieShop.Models;

namespace BethanysPieShop.Repositories
{
    public interface IOrderRepository
    {
        bool CreateOrder(Order order);
    }
}
