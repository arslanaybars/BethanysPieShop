﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BethanysPieShop.DbContext;
using BethanysPieShop.Models;
using Microsoft.EntityFrameworkCore;

namespace BethanysPieShop.Repositories
{
    public class PieRepository : IPieRepository
    {
        private readonly AppDbContext _appDbContext;

        public PieRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Pie> Pies => _appDbContext.Pies.Include(c => c.Category);

        public IEnumerable<Pie> PiesOfTheWeek
        {
            get { return _appDbContext.Pies.Include(c => c.Category).Where(pie => pie.IsPieOfTheWeek); }
        }

        public Pie GetPieById(int pieId)
        {
            return _appDbContext.Pies.FirstOrDefault(pie => pie.PieId == pieId);
        }
    }
}
